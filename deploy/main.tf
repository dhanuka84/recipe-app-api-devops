terraform {
  backend "s3" {
    bucket         = "tf-s3-lta"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }


}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.21.0"
}


locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }

}

# ... existing code ...

data "aws_region" "current" {}

