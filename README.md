# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

## In case if can't stop docker containers : docker-compose down
## use sudo killall containerd-shim and then run down command
## if standard_init_linux.go:190: exec user process caused "no such file or directory"
## https://stackoverflow.com/questions/51508150/standard-init-linux-go190-exec-user-process-caused-no-such-file-or-directory
## find . -type f -print0 | xargs -0 dos2unix
## docker-compose -f docker-compose-proxy.yml up --build

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000
