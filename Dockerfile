FROM python:3.7-alpine
LABEL maintainer="London App Developer Ltd"

ENV GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GOARCH=wasm GOOS=js


ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"

RUN pip install --upgrade pip


COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app
COPY ./scripts/ /scripts/
RUN chmod +x /scripts/*
RUN chmod +x /app/app/*.py
RUN chmod +x /app/core/*.py
RUN chmod +x /app/recipe/*.py
RUN chmod +x /app/user/*.py
RUN chmod +x /app/*.py

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chown -R user:user /scripts/
RUN chown -R user:user  /app/
RUN chmod -R 755 /vol/web

USER user

ENTRYPOINT ["sh" ,"/scripts/entrypoint.sh"]
